# Food Search  Service

Spring Boot REST Service to search for food items on a third FatsecretService API.

## Features

* offer an own REST interface with two GET endpoints
* call closed FatsecretService API to get data
* map FatsecretService FoodItem Object to domain object
* produce JSON response to show data

## Challenge 

**Create REST Service to call FatsecretService API**

Features

* Create  spring-boot-application
* Integrate the FatSecret Java API http://www.fatsecret4j.com/java
* Implement search input field in REST for the user to search for Food items
* implement a REST Interface to load the found Food item

Framework conditions

* You can use whatever framework you like. We mainly use Angular. The only condition is spring-boot
* you have access to needed data via FatSecret API. For the use of FatSecret API you need to register on the following page: https://platform.fatsecret.com/api/Default.aspx?screen=r
* you get access with the menu item "My Account- Overview"
* create a public Git repository (e.g. GitHub) and send us the link to deliver your result.
* please commit in small iterations and take care of meaningful commit messages
* create README file, including a manual for starting the app.

## Requirements for Linux user 

### Setup

* Clone the application:

`git clone git@gitlab.com:locke-code-challenges/food-search.git`

* credentilsopen src/main/resources/application.properties

change `foodsearch.fatsecretservice.key` and `foodsearch.fatsecretservice.secret` to your own FatsecretService credentials 

* Build and run the app using maven

$ `mvn clean install spring-boot:run`

## Explore the REST API

The service is reachable at `http://localhost:8080` and offers the following endpoints.

### Endpoints

* Get a map of available generic food items 

`GET /api/v1/food/fooditems/{foodItemQuery}`

* Get information about a single food item 

`GET /api/v1/food/fooditem/{id}`