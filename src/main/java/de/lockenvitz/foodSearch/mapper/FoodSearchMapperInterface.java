package de.lockenvitz.foodSearch.mapper;

import com.fatsecret.platform.model.Food;
import de.lockenvitz.foodSearch.model.FoodItem;

public interface FoodSearchMapperInterface {

    FoodItem map(final Food food);
}
