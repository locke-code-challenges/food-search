package de.lockenvitz.foodSearch.mapper;

import com.fatsecret.platform.model.Food;
import de.lockenvitz.foodSearch.model.FoodItem;
import de.lockenvitz.foodSearch.model.Serving;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FoodSearchMapper implements FoodSearchMapperInterface {

    public FoodItem map(final Food food) {
        return new FoodItem(
                food.getId(),
                food.getName(),
                food.getUrl(),
                food.getType(),
                food.getDescription(),
                food.getBrandName(),
                getServings(food));
    }

    private List<Serving> getServings(final Food food) {
        return food.getServings().stream().map(serving -> new Serving(
                serving.getServingId(),
                serving.getServingDescription(),
                serving.getServingUrl(),
                serving.getMetricServingUnit(),
                serving.getCalories(),
                serving.getFat(),
                serving.getSugar())
        ).collect(Collectors.toList());
    }
}
