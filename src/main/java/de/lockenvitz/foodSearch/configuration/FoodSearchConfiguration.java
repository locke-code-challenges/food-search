package de.lockenvitz.foodSearch.configuration;

import com.fatsecret.platform.services.FatsecretService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FoodSearchConfiguration {

    @Value("${foodsearch.fatsecretservice.key}")
    private String key;

    @Value("${foodsearch.fatsecretservice.secret}")
    private String secret;

    @Bean
    public FatsecretService createFatSecretService() {
        return new FatsecretService(key, secret);
    }
}
