package de.lockenvitz.foodSearch.controller;

import de.lockenvitz.foodSearch.model.FoodItem;
import de.lockenvitz.foodSearch.service.FoodSearchInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/food")
public class FoodSearchController {

    private static final Logger logger = LoggerFactory.getLogger(FoodSearchController.class);
    private final FoodSearchInterface foodSearchService;

    public FoodSearchController(final FoodSearchInterface foodSearchService) {
        this.foodSearchService = foodSearchService;
    }

    @GetMapping(value = "/fooditems/{foodItemQuery}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<Long, String>> getFoodItems(@PathVariable final String foodItemQuery) {
        final Map<Long, String> foodItemMap = foodSearchService.searchFoodItems(foodItemQuery);
        if (foodItemMap.isEmpty()) {
            logger.info("No generic food item with the name: " + foodItemQuery + " found");
        }

        return ResponseEntity.ok(foodItemMap);
    }

    @GetMapping(value = "/fooditem/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FoodItem> getFoodItem(@PathVariable final Long id) {
        final Optional<FoodItem> foodItem = foodSearchService.searchFoodItem(id);

        if (foodItem.isEmpty()) {
            logger.error("No food with id: " + id + " found");
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(foodItem.get());
    }
}
