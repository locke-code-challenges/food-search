package de.lockenvitz.foodSearch.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Serving {

    private Long servingId;
    private String servingDescription;
    private String servingUrl;
    private String metricServingUnit;
    private BigDecimal calories;
    private BigDecimal fat;
    private BigDecimal sugar;

    public Serving(
            final Long servingId,
            final String servingDescription,
            final String servingUrl,
            final String metricServingUnit,
            final BigDecimal calories,
            final BigDecimal fat,
            final BigDecimal sugar) {
        this.servingId = servingId;
        this.servingDescription = servingDescription;
        this.servingUrl = servingUrl;
        this.metricServingUnit = metricServingUnit;
        this.calories = calories;
        this.fat = fat;
        this.sugar = sugar;
    }

    public Long getServingId() {
        return servingId;
    }

    public void setServingId(final Long servingId) {
        this.servingId = servingId;
    }

    public String getServingDescription() {
        return servingDescription;
    }

    public void setServingDescription(final String servingDescription) {
        this.servingDescription = servingDescription;
    }

    public String getServingUrl() {
        return servingUrl;
    }

    public void setServingUrl(final String servingUrl) {
        this.servingUrl = servingUrl;
    }

    public String getMetricServingUnit() {
        return metricServingUnit;
    }

    public void setMetricServingUnit(final String metricServingUnit) {
        this.metricServingUnit = metricServingUnit;
    }

    public BigDecimal getCalories() {
        return calories;
    }

    public void setCalories(final BigDecimal calories) {
        this.calories = calories;
    }

    public BigDecimal getFat() {
        return fat;
    }

    public void setFat(final BigDecimal fat) {
        this.fat = fat;
    }

    public BigDecimal getSugar() {
        return sugar;
    }

    public void setSugar(final BigDecimal sugar) {
        this.sugar = sugar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Serving serving = (Serving) o;
        return Objects.equals(servingId, serving.servingId) &&
                Objects.equals(servingDescription, serving.servingDescription) &&
                Objects.equals(servingUrl, serving.servingUrl) &&
                Objects.equals(metricServingUnit, serving.metricServingUnit) &&
                Objects.equals(calories, serving.calories) &&
                Objects.equals(fat, serving.fat) &&
                Objects.equals(sugar, serving.sugar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(servingId, servingDescription, servingUrl, metricServingUnit, calories, fat, sugar);
    }
}
