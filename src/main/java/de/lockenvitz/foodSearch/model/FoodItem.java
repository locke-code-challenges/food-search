package de.lockenvitz.foodSearch.model;

import java.util.List;
import java.util.Objects;

public class FoodItem {

    private Long id;
    private String name;
    private String url;
    private String type;
    private String description;
    private String brandName;
    private List<Serving> servings;

    public FoodItem(
            final Long id,
            final String name,
            final String url,
            final String type,
            final String description,
            final String brandName,
            final List<Serving> servings) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.type = type;
        this.description = description;
        this.brandName = brandName;
        this.servings = servings;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(final String brandName) {
        this.brandName = brandName;
    }

    public List<Serving> getServings() {
        return servings;
    }

    public void setServings(final List<Serving> servings) {
        this.servings = servings;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodItem foodItem = (FoodItem) o;
        return Objects.equals(id, foodItem.id) &&
                Objects.equals(name, foodItem.name) &&
                Objects.equals(url, foodItem.url) &&
                Objects.equals(type, foodItem.type) &&
                Objects.equals(description, foodItem.description) &&
                Objects.equals(brandName, foodItem.brandName) &&
                Objects.equals(servings, foodItem.servings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, url, type, description, brandName, servings);
    }
}
