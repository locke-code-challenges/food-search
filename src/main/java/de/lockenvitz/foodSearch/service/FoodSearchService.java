package de.lockenvitz.foodSearch.service;

import com.fatsecret.platform.model.CompactFood;
import com.fatsecret.platform.model.Food;
import com.fatsecret.platform.services.FatsecretService;
import com.fatsecret.platform.services.Response;
import de.lockenvitz.foodSearch.mapper.FoodSearchMapper;
import de.lockenvitz.foodSearch.mapper.FoodSearchMapperInterface;
import de.lockenvitz.foodSearch.model.FoodItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FoodSearchService implements FoodSearchInterface {

    private static final Logger logger = LoggerFactory.getLogger(FoodSearchService.class);
    private static final FoodSearchMapperInterface foodSearchMapper = new FoodSearchMapper();

    private FatsecretService service;

    public FoodSearchService(final FatsecretService service) {
        this.service = service;
    }

    @Override
    public Map<Long, String> searchFoodItems(final String foodName) {
        final Response<CompactFood> response = service.searchFoods(foodName);
        Map<Long, String> foodItemsMap = Collections.emptyMap();

        if (response != null) {
            List<CompactFood> results = response.getResults();

            foodItemsMap = (results != null)
                    ? results.stream().collect(Collectors.toMap(CompactFood::getId, e -> String.valueOf(e.getName())))
                    : Collections.emptyMap();
        } else {
            logger.error("No results could be found. This could be a problem with your credentials. Please check if " +
                    "these are correct.");
        }

        return foodItemsMap;
    }

    @Override
    public Optional<FoodItem> searchFoodItem(final Long foodId) {
        final Food food = service.getFood(foodId);
        return (food != null) ? Optional.of(foodSearchMapper.map(food)) : Optional.empty();
    }
}
