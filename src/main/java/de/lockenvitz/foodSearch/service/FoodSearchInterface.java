package de.lockenvitz.foodSearch.service;

import de.lockenvitz.foodSearch.model.FoodItem;

import java.util.Map;
import java.util.Optional;

public interface FoodSearchInterface {

    Map<Long, String> searchFoodItems(final String foodName);
    Optional<FoodItem> searchFoodItem(final Long foodId);

}
