package de.lockenvitz.foodSearch.mapper;

import com.fatsecret.platform.model.Food;
import de.lockenvitz.foodSearch.base.BaseTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FoodSearchMapperTest extends BaseTest {

    private FoodSearchMapperInterface classUnderTest = new FoodSearchMapper();

    @Test
    void map_should_map_Food_to_FoodItem() {
        final var foodItem = classUnderTest.map(getFoodItem());

        assertThat(foodItem.getId(), is(EXPECTED_FOOD_ITEM.getId()));
        assertThat(foodItem.getName(), is(EXPECTED_FOOD_ITEM.getName()));
        assertThat(foodItem.getUrl(), is(EXPECTED_FOOD_ITEM.getUrl()));
        assertThat(foodItem.getBrandName(), is(EXPECTED_FOOD_ITEM.getBrandName()));
        assertThat(foodItem.getDescription(), is(EXPECTED_FOOD_ITEM.getDescription()));
        assertThat(foodItem.getType(), is(EXPECTED_FOOD_ITEM.getType()));
        assertThat(foodItem.getServings(), is(EXPECTED_FOOD_ITEM.getServings()));
    }

    private Food getFoodItem() {
        final Food food = new Food();
        food.setId(FOOD_ID);
        food.setName(SPAGHETTI);
        food.setUrl(FOOD_URL);
        food.setBrandName(FOOD_BRAND_NAME);
        food.setDescription(FOOD_DESCRIPTION);
        food.setType(FOOD_TYPE);
        food.setServings(List.of(getServing(SERVING_ID_1), getServing(SERVING_ID_2)));
        return food;
    }
}