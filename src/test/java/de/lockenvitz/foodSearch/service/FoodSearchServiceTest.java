package de.lockenvitz.foodSearch.service;

import com.fatsecret.platform.model.CompactFood;
import com.fatsecret.platform.model.Food;
import com.fatsecret.platform.services.FatsecretService;
import com.fatsecret.platform.services.Response;
import de.lockenvitz.foodSearch.base.BaseTest;
import de.lockenvitz.foodSearch.model.FoodItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FoodSearchServiceTest extends BaseTest {

    private FoodSearchInterface classUnderTest;
    private FatsecretService fatsecretService;

    @BeforeEach
    public void setUp() {
        fatsecretService = mock(FatsecretService.class);
        classUnderTest = new FoodSearchService(fatsecretService);
    }

    @Test
    void searchFoodItems_should_return_list_of_generic_food() {
        when(fatsecretService.searchFoods(PASTA)).thenReturn(getCompactFoodResponse());

        Map<Long, String> result = classUnderTest.searchFoodItems(PASTA);

        assertThat(result.size(), is(4));
        assertThat(result, instanceOf(Map.class));
        assertThat(result, is(EXPECTED_GENERIC_FOOD_MAP));
    }

    @Test
    void searchFoodItems_should_return_empty_map() {
        when(fatsecretService.searchFoods(PASTA)).thenReturn(new Response<>());

        Map<Long, String> result = classUnderTest.searchFoodItems(PASTA);

        assertThat(result.size(), is(0));
        assertThat(result, instanceOf(Map.class));
        assertThat(result, is(Collections.emptyMap()));
    }

    @Test
    void searchFoodItems_should_return_empty_map_if_credential_incorrect() {
        when(fatsecretService.searchFoods(PASTA)).thenReturn(null);

        Map<Long, String> result = classUnderTest.searchFoodItems(PASTA);

        assertThat(result.size(), is(0));
        assertThat(result, instanceOf(Map.class));
        assertThat(result, is(Collections.emptyMap()));
    }

    @Test
    void searchFoodItem_should_return_FoodItem() {
        when(fatsecretService.getFood(FOOD_ID)).thenReturn(createFood());

        Optional<FoodItem> result = classUnderTest.searchFoodItem(FOOD_ID);

        assertThat(result, is(not(Optional.empty())));

        if (result.isPresent()){
            final FoodItem foodItem = result.get();
            assertThat(foodItem, is(EXPECTED_FOOD_ITEM));
        }
    }

    @Test
    void searchFoodItem_should_return_optional_empty() {
        when(fatsecretService.getFood(FOOD_ID)).thenReturn(null);

        Optional<FoodItem> result = classUnderTest.searchFoodItem(FOOD_ID);

        assertThat(result, is(Optional.empty()));
    }

    private Food createFood() {
        var food = new Food();
        food.setId(FOOD_ID);
        food.setBrandName(FOOD_BRAND_NAME);
        food.setDescription(FOOD_DESCRIPTION);
        food.setName(SPAGHETTI);
        food.setType(FOOD_TYPE);
        food.setUrl(FOOD_URL);
        food.setServings(List.of(getServing(SERVING_ID_1), getServing(SERVING_ID_2)));
        return food;
    }

    private Response<CompactFood> getCompactFoodResponse() {
        final Response<CompactFood> response = new Response<>();
        response.setResults(List.of(
                getCompactFood(1L, PENNE),
                getCompactFood(2L, SPAGHETTI),
                getCompactFood(3L, ROTINI),
                getCompactFood(4L, PASTA_MEAT))
        );
        return response;
    }

    private CompactFood getCompactFood(final Long id, final String name) {
        var compactFood = new CompactFood();
        compactFood.setId(id);
        compactFood.setName(name);
        return compactFood;
    }
}