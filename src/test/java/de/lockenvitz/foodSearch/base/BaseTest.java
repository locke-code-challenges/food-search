package de.lockenvitz.foodSearch.base;

import de.lockenvitz.foodSearch.model.FoodItem;
import de.lockenvitz.foodSearch.model.Serving;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class BaseTest {

    protected static final String PASTA = "pasta";
    protected static final String SPAGHETTI = "Spaghetti";
    protected static final String PENNE = "Penne";
    protected static final String ROTINI = "Rotini";
    protected static final String PASTA_MEAT = "Pasta with Meat Sauce";
    protected static final Map<Long, String> EXPECTED_GENERIC_FOOD_MAP = Map.of(
            1L, PENNE,
            2L, SPAGHETTI,
            3L, ROTINI,
            4L, PASTA_MEAT);

    protected static final long SERVING_ID_1 = 15834L;
    protected static final long SERVING_ID_2 = 16289L;
    protected static final String SERVING_URL = "https://www.fatsecret.com/calories-nutrition/generic/spaghetti-cooked?portionid=15834&portionamount=1.000";
    protected static final String SERVING_DESCRIPTION = "serving description";
    protected static final String SERVING_METRIC = "g";
    protected static final String SERVING_CALORIES = "200";
    protected static final String SERVING_FAT = "1.29";
    protected static final String SERVING_SUGAR = "0.78";

    protected static final Serving SERVING_1 = new Serving(
            SERVING_ID_1,
            SERVING_DESCRIPTION,
            SERVING_URL,
            SERVING_METRIC,
            new BigDecimal(SERVING_CALORIES),
            new BigDecimal(SERVING_FAT),
            new BigDecimal(SERVING_SUGAR));
    protected static final Serving SERVING_2 = new Serving(
            SERVING_ID_2,
            SERVING_DESCRIPTION,
            SERVING_URL,
            SERVING_METRIC,
            new BigDecimal(SERVING_CALORIES),
            new BigDecimal(SERVING_FAT),
            new BigDecimal(SERVING_SUGAR));
    protected static final List<Serving> SERVING_LIST = List.of(SERVING_1, SERVING_2);

    protected static final Long FOOD_ID = 4424L;
    protected static final String FOOD_URL = "https://www.fatsecret.com/calories-nutrition/generic/spaghetti-cooked";
    protected static final String FOOD_TYPE = "Generic";
    protected static final String FOOD_DESCRIPTION = "tasty spaghetti";
    protected static final String FOOD_BRAND_NAME = "brandName";

    protected static final FoodItem EXPECTED_FOOD_ITEM = new FoodItem(
            FOOD_ID,
            SPAGHETTI,
            FOOD_URL,
            FOOD_TYPE,
            FOOD_DESCRIPTION,
            FOOD_BRAND_NAME,
            SERVING_LIST);

    protected com.fatsecret.platform.model.Serving getServing(final Long id) {
        final var serving = new com.fatsecret.platform.model.Serving();
        serving.setServingId(id);
        serving.setServingDescription(BaseTest.SERVING_DESCRIPTION);
        serving.setServingUrl(BaseTest.SERVING_URL);
        serving.setMetricServingUnit("g");
        serving.setCalories(new BigDecimal(BaseTest.SERVING_CALORIES));
        serving.setFat(new BigDecimal(BaseTest.SERVING_FAT));
        serving.setSugar(new BigDecimal(BaseTest.SERVING_SUGAR));
        return serving;
    }
}
