package de.lockenvitz.foodSearch.controller;

import de.lockenvitz.foodSearch.base.BaseTest;
import de.lockenvitz.foodSearch.model.FoodItem;
import de.lockenvitz.foodSearch.service.FoodSearchInterface;
import de.lockenvitz.foodSearch.service.FoodSearchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FoodSearchControllerTest extends BaseTest {

    private FoodSearchController classUnderTest;
    private FoodSearchInterface foodSearchService;

    @BeforeEach
    public void setUp() {
        foodSearchService = mock(FoodSearchService.class);
        classUnderTest = new FoodSearchController(foodSearchService);
    }

    @Test
    void getFoodItems_should_return_responseEntity_with_map_of_string() {
        when(foodSearchService.searchFoodItems(PASTA)).thenReturn(EXPECTED_GENERIC_FOOD_MAP);

        ResponseEntity<Map<Long, String>> result = classUnderTest.getFoodItems(PASTA);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EXPECTED_GENERIC_FOOD_MAP));
    }

    @Test
    void getFoodItems_should_return_responseEntity_with_empty_map() {
        when(foodSearchService.searchFoodItems(PASTA)).thenReturn(Collections.emptyMap());

        ResponseEntity<Map<Long, String>> result = classUnderTest.getFoodItems(PASTA);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(Collections.emptyMap()));
    }

    @Test
    void getFoodItem_should_return_responseEntity_with_FoodItem() {
        when(foodSearchService.searchFoodItem(FOOD_ID)).thenReturn(Optional.of(EXPECTED_FOOD_ITEM));

        ResponseEntity<FoodItem> result = classUnderTest.getFoodItem(FOOD_ID);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EXPECTED_FOOD_ITEM));
    }

    @Test
    void getFoodItem_should_return_responseEntity_with_bad_request() {
        when(foodSearchService.searchFoodItem(FOOD_ID)).thenReturn(Optional.empty());

        ResponseEntity<FoodItem> result = classUnderTest.getFoodItem(FOOD_ID);

        assertThat(result.getStatusCodeValue(), is(400));
    }
}